/*
    This file is part of libsdlvnc.

    libsdlvnc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    libsdlvnc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with libsdlvnc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __SDLVNCKEYBOARDQUEUE_H__
#define __SDLVNCKEYBOARDQUEUE_H__

#include "sdlvnc_client.h"

typedef struct sdlvnc_keyboard_data
{
    rfbKeySym key;
    rfbBool shiftdown;
} sdlvnc_keyboard_data;

#ifdef __cplusplus
extern "C" {
#endif

void rfbkey_queue(rfbClient *client, rfbKeySym key, rfbBool shiftdown);
void rfbkey_pop(rfbClient *client);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif // __SDLVNCKEYBOARDQUEUE_H__