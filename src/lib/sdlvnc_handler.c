/*
 This file is part of libsdlvnc.

 libsdlvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 libsdlvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with libsdlvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <assert.h>
#include <stdlib.h>
#include <pthread.h>
#include <SDL2/SDL_syswm.h>
#include "config.h"
#include "sdlvnc_util.h"
#include "sdlvnc_log.h"
#include "sdlvnc_thread.h"
#include "sdlvnc_handler.h"

#define SDLVNC_SDL_DATA 			"SDLVNC_DATA_SDL"	// id to data structure
#define SDLVNC_SDL_LOOP_DELAY		16					// loop delay in milliseconds
#define SDLVNC_SDL_FRAME_DELAY		16					// delay for screen update
#define SDLVNC_SDL_SIGNAL_DELAY		10					// delay for processing SDL events in milliseconds

#define SDLVNC_SDL_WAIT	1					// delay for processing signals in microseconds

typedef struct sdlvnc_sdl {
	SDL_Window *window;
	SDL_Renderer *renderer;
	SDL_Surface *surface;
	SDL_Texture *texture;
	SDL_RendererInfo *info;
	int mouseX;
	int mouseY;
	int mouseState;
	float scale, widthScale, heightScale;
	Uint32 rmask, gmask, bmask, amask;
	Uint32 pixel_format;
	int viewX, viewY, viewWidth, viewHeight, windowX, windowY, windowW, windowH;
	unsigned int bytesPerPixel;
	void *frameBufferUpdate;

	int rightAltDown, leftAltDown, rightShiftDown, leftShiftDown;

	int quit;

	Uint32 win_id;

	long frame_count;
	long frame_rate;
	long last_paint_millis;
	long last_frame_update;
	long last_buffer_update;

	SDL_Rect cursorLockRect;
	void *cursorLockPixels;
	int cursorLockPitch;

	SDL_mutex *mutex;
} sdlvnc_sdl_data;

typedef struct sdlvnc_button_map {
	int sdl;
	int rfb;
} sdlvnc_button_map;

sdlvnc_button_map buttonMapping[] = { { SDL_BUTTON_LEFT, rfbButton1Mask }, {
		SDL_BUTTON_MIDDLE, rfbButton2Mask },
		{ SDL_BUTTON_RIGHT, rfbButton3Mask }, { SDL_BUTTON_X1, rfbButton4Mask },
		{ SDL_BUTTON_X2, rfbButton5Mask }, { 0, 0 } };

sdlvnc_sdl_data *sdlvnc_get_sdl(rfbClient *client);
void sdlvnc_cursor_handle_sdl(rfbClient *client, SDL_Event *e);
int sdlvnc_cursor_grab_sdl(SDL_Window *window);
int sdlvnc_cursor_release_sdl(SDL_Window *window);
void sdlvnc_on_resize_sdl(rfbClient *client);
void sdlvnc_delay_sdl(rfbClient *client);
void sdlvnc_delay_stamp_sdl(rfbClient *client);
int sdlvnc_framerate_sdl(rfbClient *client);
void sdlvnc_framecount_reset_sdl(rfbClient *client);

rfbBool sdlvnc_mode_picker(SDL_DisplayMode *target, SDL_DisplayMode *closest) {
	int displayCount = SDL_GetNumVideoDisplays();
	int display;

	sdlvnc_print_debug("Number of displays: %d\n", displayCount);

	for (display = 0; display < displayCount; display++) {

		int modeCount = SDL_GetNumDisplayModes(display);
		int mode;

		if (!SDL_GetClosestDisplayMode(display, target, closest)) {
			return TRUE;
		}

		for (mode = 0; mode < modeCount; mode++) {
			if (!SDL_GetDisplayMode(display, mode, closest)) {
				if (closest->format == target->format && closest->w <= target->w
						&& closest->h <= target->h)
					return TRUE;
			}
		}

		if (!SDL_GetCurrentDisplayMode(display, closest)) {
			return TRUE;
		}
	}

	return FALSE;
}

rfbBool sdlvnc_initsub_sdl() {
	sdlvnc_print_debug("Initializing SDL\n");
	assert(SDL_WasInit(SDL_INIT_VIDEO) & SDL_INIT_VIDEO);

	if (SDL_EventState(SDL_SYSWMEVENT, SDL_DISABLE) == SDL_DISABLE) {
		sdlvnc_print_debug("System event is not processed\n");
	} else {
		sdlvnc_print_debug("System event is processed\n");
	}
	// Try set, if fail then ignore
	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "0"); // float number scan lines
	SDL_SetHint(SDL_HINT_FRAMEBUFFER_ACCELERATION, "1");
	SDL_SetHint(SDL_HINT_MOUSE_RELATIVE_MODE_WARP, "1"); // TODO Test this
// 	SDL_SetHint(SDL_HINT_WINDOWS_ENABLE_MESSAGELOOP, "1");
	SDL_SetHint(SDL_HINT_RENDER_VSYNC, "0");
#ifdef __ANDROID__
	SDL_SetHint(SDL_HINT_ACCELEROMETER_AS_JOYSTICK, "1"); // TODO Test Android without it
	SDL_SetHint(SDL_HINT_ANDROID_SEPARATE_MOUSE_AND_TOUCH, "1");// TODO Test Android with it
	SDL_SetHint(SDL_HINT_GRAB_KEYBOARD, "1");
	SDL_SetHint(SDL_HINT_JOYSTICK_ALLOW_BACKGROUND_EVENTS, "0");
	SDL_SetHint(SDL_HINT_ORIENTATIONS, "0");
#endif

	sdlvnc_print_debug("SDL initialized\n");

	return TRUE;
}

void sdlvnc_cursor_handle_sdl(rfbClient *client, SDL_Event *e) {
	sdlvnc_sdl_data *data = rfbClientGetClientData(client, SDLVNC_SDL_DATA);
	Uint32 windowState;
	int i;
	int count;

	// Skip if only viewing
	if (sdlvnc_is_view_only(client))
		return;

	switch (e->type) {
	case SDL_MOUSEMOTION:
		data->mouseX = e->motion.x;
		data->mouseY = e->motion.y;
		data->mouseState = e->motion.state;
		break;
	case SDL_MOUSEWHEEL:
		if (e->wheel.y > 0)
			data->mouseState |= rfbButton4Mask;
		else if (e->wheel.y < 0)
			data->mouseState |= rfbButton5Mask;

		count = e->wheel.y > 0 ? e->wheel.y : e->wheel.y * -1;

		for (i = 0; i < count; i++) {
			SendPointerEvent(client, data->mouseX, data->mouseY,
					data->mouseState);
			// Release scroll button
			SendPointerEvent(client, data->mouseX, data->mouseY,
					data->mouseState & ~(rfbButton4Mask | rfbButton5Mask));
		}
		return;
	default:
		data->mouseX = e->button.x;
		data->mouseY = e->button.y;
		for (i = 0; buttonMapping[i].sdl; i++)
			if (e->button.button == buttonMapping[i].sdl) {
				if (e->button.state == SDL_PRESSED)
					data->mouseState |= buttonMapping[i].rfb;
				else
					data->mouseState &= ~buttonMapping[i].rfb;
				break;
			}
		break;
	}

	// Only update if inside window area
	if (data->mouseX >= data->viewX && data->mouseX <= data->windowW
			&& data->mouseY >= data->viewY && data->mouseY <= data->windowH) {
		// Scale
		data->mouseX = (float) (data->mouseX - data->viewX) / data->scale;
		data->mouseY = (float) (data->mouseY - data->viewY) / data->scale;

		if (data->mouseX < 0)
			data->mouseX = 0;
		if (data->mouseX > client->width)
			data->mouseX = client->width;
		if (data->mouseY < 0)
			data->mouseY = 0;
		if (data->mouseY > client->height)
			data->mouseY = client->height;

		SendPointerEvent(client, data->mouseX, data->mouseY, data->mouseState);
	} else {
		// Release on window mode
		sdlvnc_cursor_release_sdl(data->window);
	}
}

int sdlvnc_cursor_grab_sdl(SDL_Window *window) {
	if (SDL_ShowCursor(SDL_DISABLE) < 0) {
		sdlvnc_print_error("Fail hiding cursor on window focus");
		return FALSE;
	}
	return TRUE;
}

int sdlvnc_cursor_release_sdl(SDL_Window *window) {
	if (SDL_ShowCursor(SDL_ENABLE) < 0) {
		sdlvnc_print_error("Fail showing cursor on window focus");
		return FALSE;
	}
	return TRUE;
}

void sdlvnc_cursor_lock_sdl(rfbClient *client, int x, int y, int w, int h) {
	sdlvnc_sdl_data *data = sdlvnc_get_sdl(client);

	// Area to lock
	data->cursorLockRect.x = x;
	data->cursorLockRect.y = y;
	data->cursorLockRect.w = w;
	data->cursorLockRect.h = h;

//	SDL_LockMutex(data->mutex);
	SDL_LockTexture(data->texture, &data->cursorLockRect,
			&data->cursorLockPixels, &data->cursorLockPitch);
	SDL_LockSurface(data->surface);
//	SDL_UnlockMutex(data->mutex);
}

void sdlvnc_cursor_unlock_sdl(rfbClient *client) {
	sdlvnc_sdl_data *data = sdlvnc_get_sdl(client);

	SDL_LockMutex(data->mutex);
	SDL_UnlockTexture(data->texture);
	SDL_UnlockSurface(data->surface);
	data->cursorLockPixels = NULL;
	SDL_UnlockMutex(data->mutex);
}

rfbBool sdlvnc_update_fb_sdl(rfbClient * client) {
	if (client->frameBuffer)
		free(client->frameBuffer);
	{
		size_t size;
		int byte_size = client->format.bitsPerPixel / 8;
		size = client->width * client->height * byte_size;
		sdlvnc_print_debug("frameBuffer: %d %d\n", byte_size, size);
		client->frameBuffer = malloc(size);
		assert(client->frameBuffer);
	}

	return TRUE;
}

int sdlvnc_scale_sdl(rfbClient* client) {
	sdlvnc_sdl_data *data;

	data = sdlvnc_get_sdl(client);

	if (!data)
		return FALSE;

	if (client->width != data->windowW && client->height != data->windowH) {
		// Calculate
		data->widthScale = (float) data->windowW / (float) client->width;
		data->heightScale = (float) data->windowH / (float) client->height;
		data->scale = (
				data->widthScale < data->heightScale ?
						data->widthScale : data->heightScale);

		// Scale
		data->viewWidth = (float) client->width * data->scale;
		data->viewHeight = (float) client->height * data->scale;

		// Center view
		data->viewX = (float) (data->windowW - data->viewWidth) / 2;
		data->viewY = (float) (data->windowH - data->viewHeight) / 2;

		if (!data->renderer)
			return FALSE;

		SDL_LockMutex(data->mutex);
		if (SDL_RenderClear(data->renderer) == -1) {
			sdlvnc_print_error("Fail setting floating proportion: %s\n",
					SDL_GetError());
			return FALSE;
		}

		if (SDL_RenderSetScale(data->renderer, data->scale, data->scale)
				== -1) {
			sdlvnc_print_error("Fail setting floating proportion: %s\n",
					SDL_GetError());
			return FALSE;
		}
		SDL_UnlockMutex(data->mutex);

		sdlvnc_print_debug("Scale: %02.2f (%02.2f x %02.2f)\n", data->scale,
				data->widthScale, data->heightScale);
	}

	return TRUE;
}

rfbBool sdlvnc_mallocfb_sdl(rfbClient * client) {
	assert(client);
	sdlvnc_sdl_data *data;
	SDL_DisplayMode target, closest;
	int sdlFlags;

	data = sdlvnc_get_sdl(client);
	assert(data);

	if (!sdlvnc_initsub_sdl()) {
		return FALSE;
	}

	data->viewWidth = sdlvnc_get_size(client).w;
	data->viewHeight = sdlvnc_get_size(client).h;
	data->windowW = data->viewWidth;
	data->windowH = data->viewHeight;

	if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
		data->rmask = 0xff000000;
		data->gmask = 0x00ff0000;
		data->bmask = 0x0000ff00;
		data->amask = 0x000000ff;
		data->pixel_format = SDL_PIXELFORMAT_ABGR8888;
	} else {
		data->amask = 0x000000ff;
		data->rmask = 0x0000ff00;
		data->gmask = 0x00ff0000;
		data->bmask = 0xff000000;
		data->pixel_format = SDL_PIXELFORMAT_ARGB8888;
	}
	sdlvnc_print_debug("endian: %s %s 0x%08x 0x%08x 0x%08x 0x%08x\n",
			sdlvnc_is_big_endian(client) ?
					"'client big endian'" : "'client little endian'",
			SDL_BYTEORDER == SDL_BIG_ENDIAN ?
					"'display big endian'" : "'display little endian'",
			data->rmask, data->gmask, data->bmask, data->amask);

	data->bytesPerPixel = client->format.bitsPerPixel / 8;

// Set the desired resolution, etc.
	target.w = data->windowW;
	target.h = data->windowH;
	target.format = data->pixel_format;
	target.refresh_rate = 0; // don't care
	target.driverdata = 0;   // initialize to 0

	sdlvnc_print_debug("Requesting: \t%d x %d @%d\n", target.w, target.h,
			target.refresh_rate);

	if (sdlvnc_is_fullscreen(client) && sdlvnc_mode_picker(&target, &closest)) {
		sdlvnc_print_debug("Got: \t%d x %d @%d\n", closest.w, closest.h,
				closest.refresh_rate);
	} else {
		closest.w = target.w;
		closest.h = target.h;
		closest.format = target.format;
		closest.refresh_rate = target.refresh_rate;
		closest.driverdata = target.driverdata;
	}

	sdlFlags = SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI;
	sdlFlags |= (
			sdlvnc_is_fullscreen(client) ?
					SDL_WINDOW_FULLSCREEN_DESKTOP : SDL_WINDOW_RESIZABLE);

	sdlvnc_print_debug("Creating window and renderer: %d x %d @%d %s\n",
			closest.w, closest.h, closest.refresh_rate,
			(sdlFlags & SDL_WINDOW_FULLSCREEN_DESKTOP ? "Full" : "Window"));

	if (SDL_CreateWindowAndRenderer(closest.w, closest.h, sdlFlags,
			&data->window, &data->renderer)) {
		sdlvnc_print_debug("Fail creating window: %s\n", SDL_GetError());
		return FALSE;
	}
//	data->window = SDL_CreateWindow("sdlvnc", SDL_WINDOWPOS_CENTERED,
//			SDL_WINDOWPOS_CENTERED, closest.w, closest.h, sdlFlags);
//	if (!data->window) {
//		sdlvnc_print_error("Fail creating window: %s\n", SDL_GetError());
//		return FALSE;
//	}
//
//	data->renderer = SDL_CreateRenderer(data->window, -1,
//			SDL_RENDERER_SOFTWARE | SDL_RENDERER_ACCELERATED
//					| SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_TARGETTEXTURE);
//	if (!data->renderer) {
//		sdlvnc_print_error("Fail creating renderer: %s\n", SDL_GetError());
//		SDL_DestroyWindow(data->window);
//		return FALSE;
//	}
//sdlvnc_thread_data *thread = sdlvnc_get_threading(client);
//SDLVNC_LOCK(thread);
//data->window = SDL_GL_GetCurrentWindow();
//data->window = SDL_GetGrabbedWindow();
//data->window = SDL_GetWindowFromID(data->win_id);
//SDLVNC_UNLOCK(thread);
//if (!data->window) {
//sdlvnc_print_error("Fail getting current window: %d %s\n", data->win_id, SDL_GetError());
//return FALSE;
//}
//data->renderer = SDL_GetRenderer(data->window);
//if (!data->renderer) {
//sdlvnc_print_error("Fail getting current renderer: %s\n", SDL_GetError());
//return FALSE;
//}

	sdlvnc_print_debug("Got window and renderer\n");

	void *icon;
	int icon_dim;

	icon = sdlvnc_get_icon(client, icon, &icon_dim);
	sdlvnc_set_icon_sdl(client, icon, icon_dim);

	SDL_SetWindowBordered(data->window, SDL_TRUE);

	SDL_RenderClear(data->renderer);

	SDL_GetWindowPosition(data->window, &data->windowX, &data->windowY);
	SDL_GetWindowSize(data->window, &data->windowW, &data->windowH);

	sdlvnc_print_debug("Window: %d,%d(%d x %d)\n", data->windowX, data->windowY,
			data->windowW, data->windowH);

	if (!sdlvnc_scale_sdl(client)) {
		sdlvnc_print_error("Fail proportioning view\n");
		SDL_DestroyRenderer(data->renderer);
		SDL_DestroyWindow(data->window);
		return FALSE;
	}

	sdlvnc_print_debug("Creating surface\n");

	data->surface = SDL_CreateRGBSurface(0, data->windowW, data->windowH,
			client->format.bitsPerPixel, data->rmask, data->gmask, data->bmask,
			data->amask);
	if (!data->surface) {
		sdlvnc_print_error("Fail creating surface: %s\n", SDL_GetError());
		SDL_DestroyRenderer(data->renderer);
		SDL_DestroyWindow(data->window);
		return FALSE;
	}

	if (SDL_SetSurfaceBlendMode(data->surface, SDL_BLENDMODE_NONE) < 0) {
		sdlvnc_print_debug("Fail setting blend mode\n");
		SDL_DestroyRenderer(data->renderer);
		SDL_DestroyWindow(data->window);
		SDL_FreeSurface(data->surface);
		return FALSE;
	}

	// { SDL_TEXTUREACCESS_STATIC | SDL_TEXTUREACCESS_STREAMING | SDL_TEXTUREACCESS_TARGET }
	data->texture = SDL_CreateTexture(data->renderer, data->pixel_format,
			SDL_TEXTUREACCESS_STREAMING, client->width, client->height);
	if (!data->texture) {
		sdlvnc_print_error("Fail creating texture: %s\n", SDL_GetError());
		SDL_DestroyRenderer(data->renderer);
		SDL_DestroyWindow(data->window);
		SDL_FreeSurface(data->surface);
		return FALSE;
	}

	if (!sdlvnc_update_client(client)) {
		sdlvnc_print_error("Fail updating client!\n");
		return FALSE;
	}

	// Allocate client buffer
	if (!sdlvnc_update_fb_sdl(client)) {
		sdlvnc_print_error("Fail allocating buffer!\n");
		return FALSE;
	}

	return TRUE;
}

void sdlvnc_paint_sdl(rfbClient *client, int x, int y, int w, int h) {
	sdlvnc_sdl_data *data;
	SDL_Rect texDest;
	SDL_Rect texSrc;
	SDL_Rect rendDst;
	void *update;
	int pitch;

	texDest.x = x;
	texDest.y = y;
	texDest.w = w;
	texDest.h = h;

	texSrc.x = texDest.x;
	texSrc.y = texDest.y;
	texSrc.w = texDest.w;
	texSrc.h = texDest.h;

	data = rfbClientGetClientData(client, SDLVNC_SDL_DATA);

	rendDst.x = texSrc.x + data->viewX;
	rendDst.y = texSrc.y + data->viewY;
	rendDst.w = texSrc.w;
	rendDst.h = texSrc.h;

	// To copy updated buffer
	update = sdlvnc_fbcpy(client, data->bytesPerPixel, x, y, w, h, update,
			&pitch);

	// To update only the changed area, use the only the area of the frame-buffer
//    if (SDL_LockTexture(texture, &src, (void**) &cl->frameBuffer, &pitch)) { // TODO windows
	if (SDL_UpdateTexture(data->texture, &texDest, update, pitch) == -1) {
		sdlvnc_print_error("SDL Error: %s (%i) 0x%x(%s)\n", SDL_GetError(),
				pitch, update, (update == NULL ? "NULL" : "VALID"));

		free(update);

		return;
	}
//    SDL_UnlockTexture(texture);

	free(update);

	if (SDL_RenderCopy(data->renderer, data->texture, &texSrc, &rendDst)
			== -1) {
		sdlvnc_print_error("SDL Error: %s\n", SDL_GetError());
		return;
	}

	// Embedded
//	SDL_RenderPresent(data->renderer);
//	sdlvnc_paint_buffer(client, data->surface->pixels, 0, 0, data->surface->w,
//			data->surface->h);
}

void sdlvnc_paint_final_sdl(rfbClient *client) {
	sdlvnc_sdl_data *data;

	data = sdlvnc_get_sdl(client);

	SDL_RenderPresent(data->renderer);
	// Embedded
//	sdlvnc_paint_buffer_final(client, data->surface->pixels);
}

rfbKeySym sdlvnc_sdlsym2rfbsym_sdl(rfbKeySym k, int sym, int shifted) {
	switch (sym) {
	case SDLK_BACKSPACE:
		k = XK_BackSpace;
		break;
	case SDLK_TAB:
		k = XK_Tab;
		break;
	case SDLK_CLEAR:
		k = XK_Clear;
		break;
	case SDLK_RETURN:
		k = XK_Return;
		break;
	case SDLK_PAUSE:
		k = XK_Pause;
		break;
	case SDLK_ESCAPE:
		k = XK_Escape;
		break;
	case SDLK_SPACE:
		k = XK_space;
		break;
	case SDLK_DELETE:
		k = XK_Delete;
		break;
	case SDLK_KP_0:
		k = XK_KP_0;
		break;
	case SDLK_KP_1:
		k = XK_KP_1;
		break;
	case SDLK_KP_2:
		k = XK_KP_2;
		break;
	case SDLK_KP_3:
		k = XK_KP_3;
		break;
	case SDLK_KP_4:
		k = XK_KP_4;
		break;
	case SDLK_KP_5:
		k = XK_KP_5;
		break;
	case SDLK_KP_6:
		k = XK_KP_6;
		break;
	case SDLK_KP_7:
		k = XK_KP_7;
		break;
	case SDLK_KP_8:
		k = XK_KP_8;
		break;
	case SDLK_KP_9:
		k = XK_KP_9;
		break;
	case SDLK_KP_PERIOD:
		k = XK_KP_Decimal;
		break;
	case SDLK_KP_DIVIDE:
		k = XK_KP_Divide;
		break;
	case SDLK_KP_MULTIPLY:
		k = XK_KP_Multiply;
		break;
	case SDLK_KP_MINUS:
		k = XK_KP_Subtract;
		break;
	case SDLK_KP_PLUS:
		k = XK_KP_Add;
		break;
	case SDLK_KP_ENTER:
		k = XK_KP_Enter;
		break;
	case SDLK_KP_EQUALS:
		k = XK_KP_Equal;
		break;
	case SDLK_UP:
		k = XK_Up;
		break;
	case SDLK_DOWN:
		k = XK_Down;
		break;
	case SDLK_RIGHT:
		k = XK_Right;
		break;
	case SDLK_LEFT:
		k = XK_Left;
		break;
	case SDLK_INSERT:
		k = XK_Insert;
		break;
	case SDLK_HOME:
		k = XK_Home;
		break;
	case SDLK_END:
		k = XK_End;
		break;
	case SDLK_PAGEUP:
		k = XK_Page_Up;
		break;
	case SDLK_PAGEDOWN:
		k = XK_Page_Down;
		break;
	case SDLK_F1:
		k = XK_F1;
		break;
	case SDLK_F2:
		k = XK_F2;
		break;
	case SDLK_F3:
		k = XK_F3;
		break;
	case SDLK_F4:
		k = XK_F4;
		break;
	case SDLK_F5:
		k = XK_F5;
		break;
	case SDLK_F6:
		k = XK_F6;
		break;
	case SDLK_F7:
		k = XK_F7;
		break;
	case SDLK_F8:
		k = XK_F8;
		break;
	case SDLK_F9:
		k = XK_F9;
		break;
	case SDLK_F10:
		k = XK_F10;
		break;
	case SDLK_F11:
		k = XK_F11;
		break;
	case SDLK_F12:
		k = XK_F12;
		break;
	case SDLK_F13:
		k = XK_F13;
		break;
	case SDLK_F14:
		k = XK_F14;
		break;
	case SDLK_F15:
		k = XK_F15;
		break;
	case SDLK_NUMLOCKCLEAR:
		k = XK_Num_Lock;
		break;
	case SDLK_CAPSLOCK:
		sdlvnc_print_debug("Capslock\n");
		k = XK_Caps_Lock;
		break;
	case SDLK_SCROLLLOCK:
		k = XK_Scroll_Lock;
		break;
	case SDLK_RSHIFT:
		k = XK_Shift_R;
		break;
	case SDLK_LSHIFT:
		k = XK_Shift_L;
		break;
	case SDLK_RCTRL:
		k = XK_Control_R;
		break;
	case SDLK_LCTRL:
		k = XK_Control_L;
		break;
	case SDLK_RALT:
		k = XK_Alt_R;
		break;
	case SDLK_LALT:
		k = XK_Alt_L;
		break;
	case SDLK_LGUI:
		k = XK_Super_L;
		break;
	case SDLK_RGUI:
		k = XK_Super_R;
		break;
	case SDLK_MODE:
		k = XK_Mode_switch;
		break;
	case SDLK_HELP:
		k = XK_Help;
		break;
	case SDLK_PRINTSCREEN:
		k = XK_Print;
		break;
	case SDLK_SYSREQ:
		k = XK_Sys_Req;
		break;
	default:
		break;
	}
	/* both SDL and X11 key symbols match ASCII in the range 0x01-0x7f */
	if (k == 0 && sym > 0x0 && sym < 0x100) {
		/* both SDL and X11 key symbols match ASCII in the range 0x01-0x7f */
		int k;
		k = sym;
		if (shifted) {
			if (k >= '1' && k <= '9')
				k &= ~0x10;
			else if (k >= 'a' && k <= 'f')
				k &= ~0x20;
		}
	}
	return k;
}

int sdlvnc_keyboard_sdl(rfbClient* client, sdlvnc_sdl_data *data, SDL_Event e) {
	rfbKeySym k;
	int sym;
	int shifted;

	if (sdlvnc_is_view_only(client))
		return TRUE;

	k = 0;
	sym = e.key.keysym.sym;
	shifted = e.key.keysym.mod & (KMOD_LSHIFT | KMOD_RSHIFT);

	SendKeyEvent(client, sdlvnc_sdlsym2rfbsym_sdl(k, sym, shifted),
			e.type == SDL_KEYDOWN ? TRUE : FALSE);

	if (e.key.keysym.sym == SDLK_RALT)
		data->rightAltDown = e.type == SDL_KEYDOWN;
	if (e.key.keysym.sym == SDLK_LALT)
		data->leftAltDown = e.type == SDL_KEYDOWN;
	if (e.key.keysym.sym == SDLK_RSHIFT)
		data->rightShiftDown = e.type == SDL_KEYDOWN;
	if (e.key.keysym.sym == SDLK_LSHIFT)
		data->leftShiftDown = e.type == SDL_KEYDOWN;

	switch (e.key.keysym.sym) {
	case SDL_SCANCODE_AC_BACK:
		sdlvnc_print_debug("Android back key pressed\n");
		return FALSE;
	}
	return TRUE;
}

void sdlvnc_textinput_sdl(rfbClient* client, SDL_Event* e) {
	sdlvnc_paste_text(client, e->text.text);
}

int sdlvnc_handle_sdl(rfbClient *client, SDL_Event *e) {
	sdlvnc_sdl_data *data = rfbClientGetClientData(client, SDLVNC_SDL_DATA);
	SDL_SysWMmsg *msg = e->syswm.msg;
	const char *sdlAudioDevName;
	rfbKeySym k;
	int sym;
	int shifted;
#ifdef __X11__
	XEvent x_event;
#endif

	switch (e->type) {
	case SDL_FIRSTEVENT:
		sdlvnc_print_debug("Starting SDL");
		SendFramebufferUpdateRequest(client, 0, 0, client->width,
				client->height, FALSE);
		break;
	case SDL_WINDOWEVENT:
		switch (e->window.event) {
		case SDL_WINDOWEVENT_MOVED:
			data->windowX = e->window.data1;
			data->windowY = e->window.data2;
			sdlvnc_print_debug(
					"Window %d will be moved: (%d x %d) %d,%d(%d x %d)\n",
					e->window.windowID, client->width, client->height,
					data->windowX, data->windowY, data->windowW, data->windowH);
			break;
		case SDL_WINDOWEVENT_RESIZED:
			data->windowW = e->window.data1;
			data->windowH = e->window.data2;
			sdlvnc_print_debug(
					"Window %d will be resized: (%d x %d) %d,%d(%d x %d)\n",
					e->window.windowID, client->width, client->height,
					data->windowX, data->windowY, data->windowW, data->windowH);
			sdlvnc_on_resize_sdl(client);
			break;
		case SDL_WINDOWEVENT_SIZE_CHANGED:
		case SDL_WINDOWEVENT_MAXIMIZED:
		case SDL_WINDOWEVENT_EXPOSED:
		case SDL_WINDOWEVENT_SHOWN:
			sdlvnc_print_debug(
					"Window %d will be updated: (%d x %d) %d,%d(%d x %d)\n",
					e->window.windowID, client->width, client->height,
					data->windowX, data->windowY, data->windowW, data->windowH);
			SendFramebufferUpdateRequest(client, 0, 0, client->width,
					client->height, FALSE);
			break;
		case SDL_WINDOWEVENT_CLOSE:
			sdlvnc_print_debug("Window %d closing\n", e->window.windowID);
			return FALSE;
		case SDL_WINDOWEVENT_HIDDEN:
		case SDL_WINDOWEVENT_MINIMIZED:
			sdlvnc_print_debug("Window %d hidden/minimized: %d %02d\n",
					e->window.windowID, e->window.event);
			break;
		case SDL_WINDOWEVENT_FOCUS_GAINED:
			sdlvnc_print_debug("Window %d receive keyboard focus\n",
					e->window.windowID);
			break;
		case SDL_WINDOWEVENT_ENTER:
			sdlvnc_print_debug("Mouse entering window %d\n",
					e->window.windowID);
			data->last_frame_update = curr_time_in_milli_s();
			return sdlvnc_cursor_grab_sdl(data->window);
		case SDL_WINDOWEVENT_FOCUS_LOST:
			sdlvnc_print_debug("Window %d lost keyboard focus\n",
					e->window.windowID);
			if (data->rightAltDown) {
				SendKeyEvent(client, XK_Alt_R, FALSE);
				data->rightAltDown = FALSE;
				sdlvnc_print_debug("released right Alt key\n");
			}
			if (data->leftAltDown) {
				SendKeyEvent(client, XK_Alt_L, FALSE);
				data->leftAltDown = FALSE;
				sdlvnc_print_debug("released left Alt key\n");
			}
			break;
		case SDL_WINDOWEVENT_LEAVE:
			sdlvnc_print_debug("Mouse leaving window %d\n", e->window.windowID);
			return sdlvnc_cursor_release_sdl(data->window);
		default:
			break;
		}
		break;
	case SDL_MOUSEBUTTONUP:
	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEMOTION:
	case SDL_MOUSEWHEEL:
		sdlvnc_cursor_handle_sdl(client, e);
		break;
	case SDL_KEYUP:
	case SDL_KEYDOWN:
		return sdlvnc_keyboard_sdl(client, data, *e);
	case SDL_TEXTEDITING:
		sdlvnc_print_debug("Text editing event\n");
		break;
	case SDL_TEXTINPUT:
		sdlvnc_textinput_sdl(client, e);
		return TRUE;
// 	case SDL_KEYMAPCHANGED:
// 		sdlvnc_print_debug("Key map event\n");
// 		break;
	case SDL_QUIT:
		sdlvnc_print_debug("Client quit\n");
		return FALSE;
	case SDL_APP_TERMINATING:
		sdlvnc_print_debug("Application terminating\n");
		return FALSE;
	case SDL_APP_LOWMEMORY:
		sdlvnc_print_debug("Application lacking memory\n");
		return FALSE;
	case SDL_APP_WILLENTERBACKGROUND:
	case SDL_APP_DIDENTERBACKGROUND:
		sdlvnc_print_debug("Application going background\n");
		break;
	case SDL_SYSWMEVENT:
		switch (msg->subsystem) {
		case SDL_SYSWM_WINDOWS:
			sdlvnc_print_debug("Windows syswm: 0x%x\n", msg->subsystem);
			break;
#ifdef __X11__
			case SDL_SYSWM_X11:
			x_event = msg->msg.x11.event;
			switch (x_event.type)
			{
				case NoEventMask:
				sdlvnc_print_debug("No events wanted\n");
				break;
				case KeyPressMask:
				sdlvnc_print_debug("Keyboard down events wanted\n");
				break;
				case KeyReleaseMask:
				sdlvnc_print_debug("Keyboard up events wanted\n");
				break;
				case ButtonPressMask:
				sdlvnc_print_debug("Pointer button down events wanted\n");
				break;
				case ButtonReleaseMask:
				sdlvnc_print_debug("Pointer button up events wanted\n");
				break;
				case EnterWindowMask:
				sdlvnc_print_debug("Pointer window entry events wanted\n");
				break;
				case LeaveWindowMask:
				sdlvnc_print_debug("Pointer window leave events wanted\n");
				break;
				case PointerMotionMask:
				sdlvnc_print_debug("Pointer motion events wanted\n");
				break;
				case PointerMotionHintMask:
				sdlvnc_print_debug("Pointer motion hints wanted\n");
				break;
				case Button1MotionMask:
				sdlvnc_print_debug("Pointer motion while button 1 down\n");
				break;
				case Button2MotionMask:
				sdlvnc_print_debug("Pointer motion while button 2 down\n");
				break;
				case Button3MotionMask:
				sdlvnc_print_debug("Pointer motion while button 3 down\n");
				break;
				case Button4MotionMask:
				sdlvnc_print_debug("Pointer motion while button 4 down\n");
				break;
				case Button5MotionMask:
				sdlvnc_print_debug("Pointer motion while button 5 down\n");
				break;
				case ButtonMotionMask:
				sdlvnc_print_debug("Pointer motion while any button down\n");
				break;
				case KeymapStateMask:
				sdlvnc_print_debug("Keyboard state wanted at window entry and focus in\n");
				break;
				case ExposureMask:
				sdlvnc_print_debug("Any exposure wanted\n");
				break;
				case VisibilityChangeMask:
				sdlvnc_print_debug("Any change in visibility wanted\n");
				break;
				case StructureNotifyMask:
				sdlvnc_print_debug("Any change in window structure wanted\n");
				break;
				case ResizeRedirectMask:
				sdlvnc_print_debug("Redirect resize of this window\n");
				break;
				case SubstructureNotifyMask:
				sdlvnc_print_debug("Substructure notification wanted\n");
				break;
				case SubstructureRedirectMask:
				sdlvnc_print_debug("Redirect structure requests on children\n");
				break;
				case FocusChangeMask:
				sdlvnc_print_debug("Any change in input focus wanted\n");
				break;
				case PropertyChangeMask:
				sdlvnc_print_debug("Any change in property wanted\n");
				break;
				case ColormapChangeMask:
				sdlvnc_print_debug("Any change in colormap wanted\n");
				break;
				case OwnerGrabButtonMask:
				sdlvnc_print_debug("Automatic grabs should activate with owner_events set to True\n");
				break;
				default:
				sdlvnc_print_debug("default X11 event?\n");
				break;
			}
			break;
#endif
		case SDL_SYSWM_DIRECTFB:
			sdlvnc_print_debug("DirectFB syswm: 0x%x\n", msg->subsystem);
			break;
		case SDL_SYSWM_COCOA:
			sdlvnc_print_debug("Cocoa syswm: 0x%x\n", msg->subsystem);
			break;
		case SDL_SYSWM_UIKIT:
			sdlvnc_print_debug("UIKit (iOS) syswm: 0x%x\n", msg->subsystem);
			break;
		case SDL_SYSWM_WAYLAND:
			sdlvnc_print_debug("Wayland syswm: 0x%x\n", msg->subsystem);
			break;
		case SDL_SYSWM_MIR:
			sdlvnc_print_debug("Mir syswm: 0x%x\n", msg->subsystem);
			break;
#ifdef __WINRT__
			case SDL_SYSWM_WINRT:
			sdlvnc_print_debug("WinRT syswm: 0x%x\n", msg->subsystem);
			break;
#endif
#ifdef __ANDROID__
			case SDL_SYSWM_ANDROID:
			sdlvnc_print_debug("Android syswm: 0x%x\n", msg->subsystem);
			break;
#endif
		case SDL_SYSWM_UNKNOWN:
		default:
			sdlvnc_print_debug("Unknown syswm: 0x%x\n", msg->subsystem);
			break;
		}
		break;
	case SDL_USEREVENT:
		switch (e->user.code) {
		default:
			sdlvnc_print_debug("Unknown user event: 0x%x\n", e->user.code);
			break;
		}
		break;
// 	case SDL_AUDIODEVICEADDED:
// 		// Grab audio
// 		sdlAudioDevName = SDL_GetAudioDriver(e->adevice.which);
// 		if (SDL_AudioInit(sdlAudioDevName) < 0) {
// 			sdlvnc_print_error("Error opening added audio device: %s %s\n",
// 					sdlAudioDevName, SDL_GetError());
// 			return FALSE;
// 		}
// 		break;
// 	case SDL_AUDIODEVICEREMOVED:
// 		// Release audio
// 		SDL_AudioQuit();
// 		break;
	default:
		sdlvnc_print_debug("ignore SDL event: 0x%x\n", e->type);
		break;
	}

	if (e->type & SDL_JOYAXISMOTION) {
		// sdlvnc_print_debug("SDL event: SDL_JOYAXISMOTION 0x%x\n", e->type);
	}

	return TRUE;
}

/*
 returns:
 1 = continue
 0 = break
 */
int sdlvnc_event_sdl(rfbClient *client, long millis) {
	SDL_Event e;
//	UNUSED (millis)

	if (client == NULL)
		return TRUE;

//	if (SDL_PollEvent(&e) == 1) {
	if (SDL_WaitEventTimeout(&e, millis) == 1) {
		return sdlvnc_handle_sdl(client, &e);
	}

	return TRUE;
}

int sdlvnc_start_sdl(rfbClient *client) {
	int quit = 0;
	int result = EXIT_SUCCESS;
	sdlvnc_sdl_data *data = rfbClientGetClientData(client, SDLVNC_SDL_DATA);
	assert(data);

	sdlvnc_print_debug("sdlvnc_loop_sdl loop\n");

	while (quit == 0) {
		if (!sdlvnc_event_sdl(client, SDLVNC_SDL_WAIT)) {
			break;
		}

		quit = sdlvnc_get_quit_timed(client, SDLVNC_SDL_SIGNAL_DELAY);
	}

	// In case quit comes from client interface instead of server
	sdlvnc_quit_client(client, EXIT_SUCCESS);

	sdlvnc_print_debug("sdlvnc_loop_sdl leave\n");

	return result;
}

void *sdlvnc_create_sdl() {
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		sdlvnc_print_error("Error initiating SDL: %s\n", SDL_GetError());
		return NULL;
	}

	sdlvnc_sdl_data *data = malloc(sizeof(sdlvnc_sdl_data));
	data->quit = 0;
	data->rightAltDown = FALSE;
	data->leftAltDown = FALSE;
	data->window = NULL;
	data->renderer = NULL;
	data->frame_count = 0;
	data->frame_rate = 0;
	data->cursorLockRect.x = 0;
	data->cursorLockRect.y = 0;
	data->cursorLockRect.w = 0;
	data->cursorLockRect.h = 0;
	data->cursorLockPixels = NULL;
	data->cursorLockPitch = 0;
	data->frameBufferUpdate = NULL;
	data->last_frame_update = 0;
	data->info = NULL;
	data->mutex = SDL_CreateMutex();
	data->windowX = 0;
	data->windowY = 0;
	data->windowW = 0;
	data->windowH = 0;

	return data;
}

void sdlvnc_clear_sdl(rfbClient *client) {
	sdlvnc_sdl_data *data = rfbClientGetClientData(client, SDLVNC_SDL_DATA);
	if (!data)
		return;
	if (data->texture)
		SDL_DestroyTexture(data->texture);
	if (data->surface)
		SDL_FreeSurface(data->surface);
	if (data->renderer)
		SDL_DestroyRenderer(data->renderer);
	if (data->window)
		SDL_DestroyWindow(data->window);
	if (data->info)
		free(data->info);
	if (data->frameBufferUpdate) {
		data->frameBufferUpdate = realloc(data->frameBufferUpdate, 0);
		if (data->frameBufferUpdate)
			free(data->frameBufferUpdate);
	}
	SDL_DestroyMutex(data->mutex);
	free(data);
	SDL_Quit();
}

sdlvnc_sdl_data * sdlvnc_get_sdl(rfbClient * client) {
	sdlvnc_sdl_data *data = rfbClientGetClientData(client, SDLVNC_SDL_DATA);
	if (!data) {
		data = sdlvnc_create_sdl();
		assert(data);
		rfbClientSetClientData(client, SDLVNC_SDL_DATA, data);
	}
	return data;
}

void sdlvnc_set_winid_sdl(rfbClient *client, Uint32 id) {
	sdlvnc_thread_data *thread = sdlvnc_get_threading(client);
	SDLVNC_LOCK(thread);
	sdlvnc_sdl_data *data = sdlvnc_get_sdl(client);
	data->win_id = id;
	SDLVNC_UNLOCK(thread);
}

void sdlvnc_cursor_set_sdl(rfbClient *client, int xhot, int yhot, int width,
		int height, int bytesPerPixel) {
	UNUSED (bytesPerPixel)
	SDL_Cursor *cursor;

	if (client->rcSource && client->rcMask) {
		cursor = SDL_CreateCursor(client->rcSource, client->rcMask, width,
				height, xhot, yhot);
		SDL_SetCursor(cursor);
	}
}

void sdlvnc_delay_sdl(rfbClient *client) {
	sdlvnc_sdl_data *data = sdlvnc_get_sdl(client);
	long curr_millis = curr_time_in_milli_s();
	Uint32 delay = SDLVNC_SDL_LOOP_DELAY
			- (curr_millis - data->last_paint_millis);
	if (delay > SDLVNC_SDL_LOOP_DELAY)
		delay = SDLVNC_SDL_LOOP_DELAY;
	SDL_Delay(delay);
}

void sdlvnc_delay_stamp_sdl(rfbClient *client) {
	sdlvnc_sdl_data *data = sdlvnc_get_sdl(client);
	data->last_paint_millis = curr_time_in_milli_s();
}

int sdlvnc_framerate_sdl(rfbClient *client) {
	sdlvnc_sdl_data *data = sdlvnc_get_sdl(client);
	return data->frame_rate;
}

void sdlvnc_framecount_reset_sdl(rfbClient *client) {
	sdlvnc_sdl_data *data = sdlvnc_get_sdl(client);
	data->frame_count = 0;
}

void sdlvnc_on_resize_sdl(rfbClient *client) {
	sdlvnc_sdl_data *data;

	if (sdlvnc_scale_sdl(client)) {
	}
}

void sdlvnc_set_view_size_sdl(rfbClient *client, sdlvnc_size size) {
	sdlvnc_sdl_data *data = sdlvnc_get_sdl(client);
	SDL_DisplayMode request;

	// TODO Window == View?
	data->windowW = size.w;
	data->windowH = size.h;

	sdlvnc_on_resize_sdl(client);
}

void sdlvnc_set_icon_sdl(rfbClient *client, void *icon, int dim) {
	sdlvnc_sdl_data *data;
	SDL_Surface *source, *destination;
	int rmask, gmask, bmask, amask;

	if (sdlvnc_is_big_endian(client)) {
		rmask = 0xff0000;
		gmask = 0x00ff00;
		bmask = 0x0000ff;
	} else {
		rmask = 0xff0000;
		gmask = 0x00ff00;
		bmask = 0x0000ff;
	}

	source = SDL_CreateRGBSurfaceFrom(icon, dim, dim, 24, dim * 3, rmask, gmask,
			bmask, amask);

	data = sdlvnc_get_sdl(client);

	SDL_SetWindowIcon(data->window, source);
	SDL_FreeSurface(source);
}
