/*
 This file is part of sdlvnc.

 sdlvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 sdlvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with sdlvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * sdlvnc.h
 *
 *  Created on: Oct 17, 2016
 *      Author: rona
 */

#ifndef SDLVNC_H_
#define SDLVNC_H_

#include <sdlvnc_client.h>

typedef void (*sdlvnc_clipcut_callback)(const char *text, int textlen);

#ifdef __cplusplus
extern "C" {
#endif

int sdlvnc_start(const char *hostname, int use_sdl, int win_id, void *icon,
		int icon_dim, sdlvnc_password_callback password_callback,
		sdlvnc_paint_callback paint_buffer_callback,
		sdlvnc_paint_final_callback paint_final_buffer_callback,
		sdlvnc_size_callback view_size_callback,
		sdlvnc_resize_callback view_resize_callback,
		sdlvnc_clipcut_callback text_cut_callback);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* SDLVNC_H_ */
