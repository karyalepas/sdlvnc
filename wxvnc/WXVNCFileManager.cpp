/*
 This file is part of wxvnc.

 wxvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 wxvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wxvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <wx/filesys.h>
#include <wx/stdpaths.h>
#include <wx/filename.h>
#include <wx/utils.h>
#include <wx/log.h>
#include <rfb/rfb.h>
#include "WXVNCFileManager.h"

//void storepasswd ( char *name, char *passwd)
//{
//    wxString path = wxStandardPaths::Get().GetUserLocalDataDir();
//    path = path.Append("/passwd/");
//    wxFileName pathname = wxFileName::DirName(path);
//    path = path.Append(name);
//    if (pathname.Mkdir(wxS_DIR_DEFAULT, wxPATH_MKDIR_FULL) &&
//            rfbEncryptAndStorePasswd(passwd, (char*) ((const char*)path.mb_str())) == 1)
//        wxLogError("Error storing password");
//    path = "";
//}

wxCharBuffer passwdfile(char *name) {
	wxString path = wxStandardPaths::Get().GetUserLocalDataDir();
	path = path.Append("/passwd/");
	wxString filepath;
	if (wxFileSystem().FindFileInPath(&filepath, path, name)) {
		wxCharBuffer pathBuffer = filepath.mb_str();
		return pathBuffer;
	}
	return "";
}

void rmpasswdfile(wxCharBuffer name) {
	wxCharBuffer passfile = passwdfile((char*) ((const char*) name));
	if (strlen(passfile) == 0)
		return;
	wxRemoveFile(passfile);
}
