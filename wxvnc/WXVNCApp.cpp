/*
 This file is part of wxvnc.

 wxvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 wxvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wxvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "wxgtkutils.h"
#include "wxvnc_icon.h"
#include "WXVNCApp.h"

using namespace wxvnc;

IMPLEMENT_APP_NO_MAIN (WXVNCApp)

bool WXVNCApp::OnInit() {
	// call default behaviour (mandatory)
	if (!wxApp::OnInit())
		return false;

	m_pLauncherFrame = new WXVNCLauncherFrame(NULL, wxID_ANY, wxEmptyString);
	SetTopWindow (m_pLauncherFrame);

	wxvnc_set_icon(wxvncicon256, 256, 196662);

	if (m_Host.IsEmpty()) {
		m_pLauncherFrame->Show();
	} else {
		m_pLauncherFrame->DoConnect(m_Host, "");
	}

	return true;
}

int WXVNCApp::OnExit() {
	return 0;
}

int WXVNCApp::OnRun() {
	return wxApp::OnRun();
}

void WXVNCApp::OnInitCmdLine(wxCmdLineParser& parser) {
	parser.SetDesc(g_cmdLineDesc);
}

bool WXVNCApp::OnCmdLineParsed(wxCmdLineParser& parser) {
	// to get at your unnamed parameters use
	wxArrayString files;
	for (int i = 0; i < parser.GetParamCount(); i++) {
		files.Add(parser.GetParam(i));
	}

	// then do what you need with them.
	if (files.Count() > 0) {
		m_Host = files.Item(files.Count() - 1);
		wxLogDebug("Host: %s", m_Host);
	}

	wxLogDebug("Parsed");

	return true;
}
