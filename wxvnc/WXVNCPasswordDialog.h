/*
 This file is part of wxvnc.

 wxvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 wxvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wxvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __WXVNCPASSWORDDIALOG_H__
#define __WXVNCPASSWORDDIALOG_H__

#include "PasswordDialog.h"
#include "WXVNCLauncherFrame.h"

namespace me {
namespace huedawn {
namespace wxvnc {

class WXVNCPasswordDialog: public PasswordDialog {
private:
	wxString m_sPassword;

protected:

public:
	WXVNCPasswordDialog(wxWindow* parent, wxWindowID id = wxID_ANY,
			const wxString& title = wxT("Remote Password"), const wxPoint& pos =
					wxDefaultPosition, const wxSize& size = wxDefaultSize,
			long style = wxDEFAULT_DIALOG_STYLE);
	~WXVNCPasswordDialog();

	virtual int ShowModal();
	wxString GetPassword();
};

}
}
}

namespace wxvnc = me::huedawn::wxvnc;

#endif //__WXVNCPASSWORDDIALOG_H__
