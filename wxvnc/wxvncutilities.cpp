/*
 This file is part of wxvnc.

 wxvnc is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 wxvnc is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with wxvnc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <wx/rawbmp.h>
#include <wx/log.h>
#include <wx/event.h>
#include "wxvncutilities.h"

typedef wxAlphaPixelData PixelData;

#undef HIDE_CURSOR
#undef SHOW_CURSOR
#if defined(__WXGTK__)
#define HIDE_CURSOR wxSetCursor(wxCURSOR_BLANK)
#define SHOW_CURSOR wxSetCursor(*wxSTANDARD_CURSOR)
#elif defined(__WXMSW__)
#define HIDE_CURSOR ShowCursor(0)
#define SHOW_CURSOR ShowCursor(1)
#endif

wxBitmap *RGBAtoBitmap(unsigned char *rgba, int w, int h, int big_endian) {
	wxBitmap *bitmap = new wxBitmap(w, h, 32);
	if (!((wxGDIObject*) bitmap)->Ok()) {
		delete bitmap;
		return NULL;
	}

	PixelData *bmdata = new PixelData(*bitmap);
	if (bmdata == NULL) {
		wxLogDebug(wxT("RGBAtoBitmap fail"));
		delete bitmap;
		return NULL;
	}

	PixelData::Iterator dst = bmdata->GetPixels();

	for (int y = 0; y < h; y++) {
		dst.MoveTo(*bmdata, 0, y);
		for (int x = 0; x < w; x++, dst++, rgba += 4) {
			if (big_endian) {
				dst.Alpha() = rgba[3] & 0xff;
				dst.Blue() = rgba[2] & 0xff;
				dst.Green() = rgba[1] & 0xff;
				dst.Red() = rgba[0] & 0xff;
			} else {
				dst.Alpha() = rgba[3] & 0xff;
				dst.Red() = rgba[2] & 0xff;
				dst.Green() = rgba[1] & 0xff;
				dst.Blue() = rgba[0] & 0xff;
			}
		}
	}

	delete bmdata;

	return bitmap;
}

rfbKeySym wxkeytorfbkey(int wx_key) {
	rfbKeySym rfb_key;

	switch (wx_key) {
	case WXK_CONTROL_A:
		rfb_key = XK_A;
		break;
	case WXK_CONTROL_B:
		rfb_key = XK_B;
		break;
	case WXK_CONTROL_C:
		rfb_key = XK_C;
		break;
	case WXK_CONTROL_D:
		rfb_key = XK_D;
		break;
	case WXK_CONTROL_E:
		rfb_key = XK_E;
		break;
	case WXK_CONTROL_F:
		rfb_key = XK_F;
		break;
	case WXK_CONTROL_G:
		rfb_key = XK_G;
		break;
	case WXK_CONTROL_H:
		rfb_key = XK_H;
		break;
	case WXK_CONTROL_I:
		rfb_key = XK_I;
		break;
	case WXK_CONTROL_J:
		rfb_key = XK_J;
		break;
	case WXK_CONTROL_K:
		rfb_key = XK_K;
		break;
	case WXK_CONTROL_L:
		rfb_key = XK_L;
		break;
	case WXK_CONTROL_M:
		rfb_key = XK_M;
		break;
	case WXK_CONTROL_N:
		rfb_key = XK_N;
		break;
	case WXK_CONTROL_O:
		rfb_key = XK_O;
		break;
	case WXK_CONTROL_P:
		rfb_key = XK_P;
		break;
	case WXK_CONTROL_Q:
		rfb_key = XK_Q;
		break;
	case WXK_CONTROL_R:
		rfb_key = XK_R;
		break;
	case WXK_CONTROL_S:
		rfb_key = XK_S;
		break;
	case WXK_CONTROL_T:
		rfb_key = XK_T;
		break;
	case WXK_CONTROL_U:
		rfb_key = XK_U;
		break;
	case WXK_CONTROL_V:
		rfb_key = XK_V;
		break;
	case WXK_CONTROL_W:
		rfb_key = XK_W;
		break;
	case WXK_CONTROL_X:
		rfb_key = XK_X;
		break;
	case WXK_CONTROL_Y:
		rfb_key = XK_Y;
		break;
	case WXK_CONTROL_Z:
		rfb_key = XK_Z;
		break;
		//case WXK_BACK: rfb_key = XK_BackSpace; break;
		//case WXK_TAB: rfb_key = XK_Tab; break;
	case WXK_CLEAR:
		rfb_key = XK_Clear;
		break;
		//case WXK_RETURN: rfb_key = XK_Return; break;
	case WXK_PAUSE:
		rfb_key = XK_Pause;
		break;
	case WXK_ESCAPE:
		rfb_key = XK_Escape;
		break;
	case WXK_SPACE:
		rfb_key = XK_space;
		break;
	case WXK_DELETE:
		rfb_key = XK_Delete;
		break;
	case WXK_NUMPAD0:
		rfb_key = XK_KP_0;
		break;
	case WXK_NUMPAD1:
		rfb_key = XK_KP_1;
		break;
	case WXK_NUMPAD2:
		rfb_key = XK_KP_2;
		break;
	case WXK_NUMPAD3:
		rfb_key = XK_KP_3;
		break;
	case WXK_NUMPAD4:
		rfb_key = XK_KP_4;
		break;
	case WXK_NUMPAD5:
		rfb_key = XK_KP_5;
		break;
	case WXK_NUMPAD6:
		rfb_key = XK_KP_6;
		break;
	case WXK_NUMPAD7:
		rfb_key = XK_KP_7;
		break;
	case WXK_NUMPAD8:
		rfb_key = XK_KP_8;
		break;
	case WXK_NUMPAD9:
		rfb_key = XK_KP_9;
		break;
	case WXK_NUMPAD_DECIMAL:
		rfb_key = XK_KP_Decimal;
		break;
	case WXK_NUMPAD_DIVIDE:
		rfb_key = XK_KP_Divide;
		break;
	case WXK_NUMPAD_MULTIPLY:
		rfb_key = XK_KP_Multiply;
		break;
	case WXK_NUMPAD_SUBTRACT:
		rfb_key = XK_KP_Subtract;
		break;
	case WXK_NUMPAD_ADD:
		rfb_key = XK_KP_Add;
		break;
	case WXK_NUMPAD_ENTER:
		rfb_key = XK_KP_Enter;
		break;
	case WXK_NUMPAD_EQUAL:
		rfb_key = XK_KP_Equal;
		break;
	case WXK_UP:
		rfb_key = XK_Up;
		break;
	case WXK_DOWN:
		rfb_key = XK_Down;
		break;
	case WXK_RIGHT:
		rfb_key = XK_Right;
		break;
	case WXK_LEFT:
		rfb_key = XK_Left;
		break;
	case WXK_INSERT:
		rfb_key = XK_Insert;
		break;
	case WXK_HOME:
		rfb_key = XK_Home;
		break;
	case WXK_END:
		rfb_key = XK_End;
		break;
	case WXK_PAGEUP:
		rfb_key = XK_Page_Up;
		break;
	case WXK_PAGEDOWN:
		rfb_key = XK_Page_Down;
		break;
	case WXK_F1:
		rfb_key = XK_F1;
		break;
	case WXK_F2:
		rfb_key = XK_F2;
		break;
	case WXK_F3:
		rfb_key = XK_F3;
		break;
	case WXK_F4:
		rfb_key = XK_F4;
		break;
	case WXK_F5:
		rfb_key = XK_F5;
		break;
	case WXK_F6:
		rfb_key = XK_F6;
		break;
	case WXK_F7:
		rfb_key = XK_F7;
		break;
	case WXK_F8:
		rfb_key = XK_F8;
		break;
	case WXK_F9:
		rfb_key = XK_F9;
		break;
	case WXK_F10:
		rfb_key = XK_F10;
		break;
	case WXK_F11:
		rfb_key = XK_F11;
		break;
	case WXK_F12:
		rfb_key = XK_F12;
		break;
	case WXK_F13:
		rfb_key = XK_F13;
		break;
	case WXK_F14:
		rfb_key = XK_F14;
		break;
	case WXK_F15:
		rfb_key = XK_F15;
		break;
	case WXK_NUMLOCK:
		rfb_key = XK_Num_Lock;
		break;
	case WXK_CAPITAL:
		rfb_key = XK_Caps_Lock;
		break;
	case WXK_SCROLL:
		rfb_key = XK_Scroll_Lock;
		break;
	case WXK_SHIFT:
		rfb_key = XK_Shift_L;
		break;
	case WXK_CONTROL:
		rfb_key = XK_Control_L;
		break;
	case WXK_ALT:
		rfb_key = XK_Alt_R;
		break;
	case WXK_WINDOWS_LEFT:
		rfb_key = XK_Super_L;
		break;
	case WXK_WINDOWS_RIGHT:
		rfb_key = XK_Super_R;
		break;
		//case WXK_MODE: rfb_key = XK_Mode_switch; break;
	case WXK_HELP:
		rfb_key = XK_Help;
		break;
	case WXK_PRINT:
		rfb_key = XK_Print;
		break;
		//case WXK_SYSREQ: rfb_key = XK_Sys_Req; break;
		//case WXK_BREAK: rfb_key = XK_Break; break;
	default:
		wxLogError("Unidentified scancode: %d", wx_key);
		rfb_key = 0;
		break;
	}

	return rfb_key;
}

wxString wxkeytostring(int wx_key) {
	wxString string;

	switch (wx_key) {
	case WXK_CONTROL_A:
		string = "A";
		break;
	case WXK_CONTROL_B:
		string = "B";
		break;
	case WXK_CONTROL_C:
		string = "C";
		break;
	case WXK_CONTROL_D:
		string = "D";
		break;
	case WXK_CONTROL_E:
		string = "E";
		break;
	case WXK_CONTROL_F:
		string = "F";
		break;
	case WXK_CONTROL_G:
		string = "G";
		break;
	case WXK_CONTROL_H:
		string = "H";
		break;
	case WXK_CONTROL_I:
		string = "I";
		break;
	case WXK_CONTROL_J:
		string = "J";
		break;
	case WXK_CONTROL_K:
		string = "K";
		break;
	case WXK_CONTROL_L:
		string = "L";
		break;
	case WXK_CONTROL_M:
		string = "M";
		break;
	case WXK_CONTROL_N:
		string = "N";
		break;
	case WXK_CONTROL_O:
		string = "O";
		break;
	case WXK_CONTROL_P:
		string = "P";
		break;
	case WXK_CONTROL_Q:
		string = "Q";
		break;
	case WXK_CONTROL_R:
		string = "R";
		break;
	case WXK_CONTROL_S:
		string = "S";
		break;
	case WXK_CONTROL_T:
		string = "T";
		break;
	case WXK_CONTROL_U:
		string = "U";
		break;
	case WXK_CONTROL_V:
		string = "V";
		break;
	case WXK_CONTROL_W:
		string = "W";
		break;
	case WXK_CONTROL_X:
		string = "X";
		break;
	case WXK_CONTROL_Y:
		string = "Y";
		break;
	case WXK_CONTROL_Z:
		string = "Z";
		break;
		//case WXK_BACK: string = "BackSpace; break;
		//case WXK_TAB: string = "Tab; break;
	case WXK_CLEAR:
		string = "Clear";
		break;
		//case WXK_RETURN: string = "Return; break;
	case WXK_PAUSE:
		string = "Pause";
		break;
	case WXK_ESCAPE:
		string = "Escape";
		break;
	case WXK_SPACE:
		string = "space";
		break;
	case WXK_DELETE:
		string = "Delete";
		break;
	case WXK_NUMPAD0:
		string = "KP_0";
		break;
	case WXK_NUMPAD1:
		string = "KP_1";
		break;
	case WXK_NUMPAD2:
		string = "KP_2";
		break;
	case WXK_NUMPAD3:
		string = "KP_3";
		break;
	case WXK_NUMPAD4:
		string = "KP_4";
		break;
	case WXK_NUMPAD5:
		string = "KP_5";
		break;
	case WXK_NUMPAD6:
		string = "KP_6";
		break;
	case WXK_NUMPAD7:
		string = "KP_7";
		break;
	case WXK_NUMPAD8:
		string = "KP_8";
		break;
	case WXK_NUMPAD9:
		string = "KP_9";
		break;
	case WXK_NUMPAD_DECIMAL:
		string = "KP_Decimal";
		break;
	case WXK_NUMPAD_DIVIDE:
		string = "KP_Divide";
		break;
	case WXK_NUMPAD_MULTIPLY:
		string = "KP_Multiply";
		break;
	case WXK_NUMPAD_SUBTRACT:
		string = "KP_Subtract";
		break;
	case WXK_NUMPAD_ADD:
		string = "KP_Add";
		break;
	case WXK_NUMPAD_ENTER:
		string = "KP_Enter";
		break;
	case WXK_NUMPAD_EQUAL:
		string = "KP_Equal";
		break;
	case WXK_UP:
		string = "Up";
		break;
	case WXK_DOWN:
		string = "Down";
		break;
	case WXK_RIGHT:
		string = "Right";
		break;
	case WXK_LEFT:
		string = "Left";
		break;
	case WXK_INSERT:
		string = "Insert";
		break;
	case WXK_HOME:
		string = "Home";
		break;
	case WXK_END:
		string = "End";
		break;
	case WXK_PAGEUP:
		string = "Page_Up";
		break;
	case WXK_PAGEDOWN:
		string = "Page_Down";
		break;
	case WXK_F1:
		string = "F1";
		break;
	case WXK_F2:
		string = "F2";
		break;
	case WXK_F3:
		string = "F3";
		break;
	case WXK_F4:
		string = "F4";
		break;
	case WXK_F5:
		string = "F5";
		break;
	case WXK_F6:
		string = "F6";
		break;
	case WXK_F7:
		string = "F7";
		break;
	case WXK_F8:
		string = "F8";
		break;
	case WXK_F9:
		string = "F9";
		break;
	case WXK_F10:
		string = "F10";
		break;
	case WXK_F11:
		string = "F11";
		break;
	case WXK_F12:
		string = "F12";
		break;
	case WXK_F13:
		string = "F13";
		break;
	case WXK_F14:
		string = "F14";
		break;
	case WXK_F15:
		string = "F15";
		break;
	case WXK_NUMLOCK:
		string = "Num_Lock";
		break;
	case WXK_CAPITAL:
		string = "Caps_Lock";
		break;
	case WXK_SCROLL:
		string = "Scroll_Lock";
		break;
	case WXK_SHIFT:
		string = "Shift_L";
		break;
	case WXK_CONTROL:
		string = "Control_L";
		break;
	case WXK_ALT:
		string = "Alt_R";
		break;
	case WXK_WINDOWS_LEFT:
		string = "Super_L";
		break;
	case WXK_WINDOWS_RIGHT:
		string = "Super_R";
		break;
		//case WXK_MODE: string = "Mode_switch; break;
	case WXK_HELP:
		string = "Help";
		break;
	case WXK_PRINT:
		string = "Print";
		break;
		//case WXK_SYSREQ: string = "Sys_Req; break;
		//case WXK_BREAK: string = "Break; break;
	default:
		wxLogDebug("Unidentified scan-code: %d", wx_key);
		string = "";
		break;
	}

	return string;
}

rfbKeySym wxunicodetorfbkey(wxChar unicode, bool shiftdown) {
	int rfb_key = unicode;
	if (unicode >= '!' && unicode <= '@') {
		if (shiftdown) {
			switch (unicode) {
			case '0':
				rfb_key = ')';
				break;
			case '1':
				rfb_key = '!';
				break;
			case '2':
				rfb_key = '@';
				break;
			case '3':
				rfb_key = '#';
				break;
			case '4':
				rfb_key = '$';
				break;
			case '5':
				rfb_key = '%';
				break;
			case '6':
				rfb_key = '^';
				break;
			case '7':
				rfb_key = '&';
				break;
			case '8':
				rfb_key = '*';
				break;
			case '9':
				rfb_key = '(';
				break;
			case '-':
				rfb_key = '_';
				break;
			case ';':
				rfb_key = ':';
				break;
			case ',':
				rfb_key = '<';
				break;
			case '=':
				rfb_key = '+';
				break;
			case '.':
				rfb_key = '>';
				break;
			case '/':
				rfb_key = '?';
				break;
			case '\'':
				rfb_key = '"';
				break;
			default:
				if (rfb_key >= '1' && rfb_key <= '5')
					rfb_key -= 0x20;
				break;
			}
		}
		return rfb_key;
	} else if (unicode >= 'A' && unicode <= 'Z' && !shiftdown) {
		rfb_key |= 0x20;
		return rfb_key;
	} else if (unicode >= '[' && unicode <= ']' && shiftdown) {
		rfb_key |= 0x20;
		return rfb_key;
	} else if (shiftdown && unicode == '`') {
		rfb_key = '~';
		return rfb_key;
	} else {
		switch (unicode) {
		case 8:
			rfb_key = XK_BackSpace;
			break;
		case 9:
			rfb_key = XK_Tab;
			break;
		case 13:
			rfb_key = XK_Return;
			break;
		case 27:
			rfb_key = XK_Escape;
			break;
		case 32:
			rfb_key = XK_space;
			break;
		case 127:
			rfb_key = XK_Delete;
			break;
		default:
			rfb_key = unicode;
			break;
		}
	}
	return rfb_key;
}

void checkIdle() {
	wxIdleEvent *idle = new wxIdleEvent();
	while (idle->MoreRequested())
		idle->RequestMore();
	delete idle;
}
