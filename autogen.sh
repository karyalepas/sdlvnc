#! /bin/sh

aclocal --force --install \
    && automake --force --add-missing --copy \
    && autoconf --force \
    && ./configure --enable-maintainer-mode "$@"
