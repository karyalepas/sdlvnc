LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := sdlvncclient-android

SDL_SRC := ../../../../../../../hg/sdl

LOCAL_C_INCLUDES := $(SDL_SRC)/include
  #\
  #../../../../project/include \
  #/home/rona/toolchain-arm/sysroot/usr/include \
  #/home/rona/toolchain-armv7/sysroot/usr/include \
  #/home/rona/toolchain-x86/sysroot/usr/include \
  #/home/rona/toolchain-arm/sysroot/usr/include/SDL2
  #/home/rona/toolchain-armv7/sysroot/usr/include/SDL2
  #/home/rona/toolchain-x86/sysroot/usr/include/SDL2

# Add your application source files here...
LOCAL_SRC_FILES := android_jni.cpp
  #../../../../project/common/ExternalInterface.cpp \
  #../../../../project/common/SDLvncviewer.c

#LOCAL_LDLIBS := \
#  -L/home/rona/toolchain-arm/sysroot/usr/lib \
#  -L/home/rona/toolchain-armv7/sysroot/usr/lib \
#  -L/home/rona/toolchain-x86/sysroot/usr/lib

#LOCAL_SHARED_LIBRARIES := SDL2

LOCAL_CFLAGS += -DGL_GLEXT_PROTOTYPES

LOCAL_LDLIBS := -ldl -lGLESv1_CM -lGLESv2 -llog -landroid
  #\
  #-l:libvncclient.a -l:libpng.a -l:libjpeg.a -l:libgcrypt.a -l:libssl.a -l:libcrypto.a  -l:libgpg-error.a -l:libSDL2.a

#LOCAL_EXPORT_LDLIBS := -Wl,--undefined=Java_org_libsdl_app_SDLActivity_nativeInit -ldl -lGLESv1_CM -lGLESv2 -llog -landroid
LOCAL_EXPORT_LDLIBS := -ldl -lGLESv1_CM -lGLESv2 -llog -landroid

include $(BUILD_SHARED_LIBRARY)
