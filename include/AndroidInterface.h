#ifndef ANDROIDINTERFACECPP_H
#define ANDROIDINTERFACECPP_H


#ifdef __cplusplus
extern "C" {
#endif


const char* Java_me_huedawn_sdlvncclient_android_SDLVNCActivity_getHostname ();
const char* Java_me_huedawn_sdlvncclient_android_SDLVNCActivity_getPassword ();


#ifdef __cplusplus
}
#endif


#endif
